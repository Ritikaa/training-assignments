package UtilApi;

import org.apache.commons.lang3.StringUtils;

public class DemoUtil {

    public static void main(String args[]) {

        String str1="Ritika";
        String str2="ritika";
        if (StringUtils.compare(str1,str2)==0) {
            System.out.println(" Output of the function compare is true");
        } // returns true
        else
        {
            System.out.println("Output of the function compare is false");
        }


        if (StringUtils.compareIgnoreCase(str1,str2)==0) {
            System.out.println("Output of the function compareIgnoreCase is true");
        } // returns true
        else
        {
            System.out.println("Output of the function compareIgnoreCase is false");
        }

        String str3="";
        if(StringUtils.isEmpty(str3))
        {
            System.out.println("string is empty");
        }
        else
        {
            System.out.println("String is not empty");
        }

        String name1="Welcome";
        String name2="Welcome";

        System.out.println("Output of equals method ");
        if(StringUtils.equals(name1,name2))
        {
            System.out.println(name1 + " " + "is eqaul to " + name2);
        }
        else
        {
            System.out.println(name1 + "is not eqaul to " + name2);
        }

        System.out.println("Output of equalsIgnoreCase");
        if(StringUtils.equalsIgnoreCase(name1,name2))
        {
            System.out.println(name1  + " " + "is eqaul to " + name2);
        }
        else
        {
            System.out.println(name1 + "is not eqaul to " + name2);
        }


        System.out.println("Output of equalsAnyIgnoreCase");
        if(StringUtils.equalsAnyIgnoreCase(name1,name2)) //equalsInoreCase
        {

            System.out.println(name1 + "" + "is eqaul to " + name2);
        }
        else
        {
            System.out.println(name1 + "is not eqaul to " + name2);
        }

        String str5=" Java Programming ";
        System.out.println("string 5 is "+ str5);
        System.out.println("Output of UpperCase function");// upper case function
        System.out.println(StringUtils.upperCase(str5));

        System.out.println("Output of trim function"); //trim function
        System.out.println(StringUtils.trim(" Java Programming "));
        // System.out.println("Trimmed string is" + str6);


        System.out.println("Abbrevation method");
        String str7="Ritika Agarwal ";
        String result=StringUtils.abbreviate(str7,8); //abbreviate method
        String res=StringUtils.abbreviate(str7,5);

        System.out.println(result);
        System.out.println(res);

        System.out.println("***************************");
        String str10="GoodBye";
        System.out.println(StringUtils.appendIfMissing(str10,"s"));

        System.out.println("Output of reverse");
        System.out.println(StringUtils.reverse(str10)); //reverse

        System.out.println("Output of chop");
        System.out.println(StringUtils.chop(str10));  //chop remove the last part of the string

        System.out.println(StringUtils.swapCase("ThIs is FuNny"));

        String lower="fff";
        if(StringUtils.isAllLowerCase(lower))
            System.out.println("true");
        else
            System.out.println("false");

        String upper="RRR";
        if(StringUtils.isAllUpperCase(upper))
            System.out.println("true");
        else
            System.out.println("false");


        System.out.println(StringUtils.isAlpha("abc"));
        System.out.println(StringUtils.isAlpha("A2BC"));
        System.out.println(StringUtils.isAlphaSpace("ab c"));
        System.out.println("Output of isAlpha Space where only alphabets are present");
        System.out.println(StringUtils.isAlphaSpace("ab 2c"));
        System.out.println(StringUtils.isAlphanumeric("abc233"));
        System.out.println(StringUtils.isAlphanumeric("-2"));


        System.out.println("Split string");
        String str="It is a Java code";
        System.out.println(str);
        System.out.println("Splitting over str");
        String[] strs=StringUtils.split(str," ",3);
        for(String s:strs){
            System.out.println(s);
        }

        System.out.println("Slitting over string without max parameter");
        String[] strs1=StringUtils.split(str," ");
        for(String s:strs1){
            System.out.println(s);
        }

        System.out.println("Implementing strips");
        String xx="java to java";
        System.out.println("Before strip" + " " + xx);
        String s=StringUtils.strip(xx,"aj");
        System.out.println("After strip" + " " +  s);


        System.out.println("Repeat");
        String repeat="Rinu";
        System.out.println(StringUtils.repeat(repeat,"-", 6));
        System.out.println(StringUtils.repeat(repeat,4));

        System.out.println("Remove end");
        String removeEnd="This is an Example";
        System.out.println(StringUtils.removeEnd(removeEnd,"Example"));

        System.out.println(StringUtils.removeEnd(removeEnd,"The"));

        System.out.println(StringUtils.getCommonPrefix("abc","pqr")); //not working

        System.out.println(StringUtils.deleteWhitespace("null"));
        System.out.println(StringUtils.deleteWhitespace("a b c"));

        System.out.println(StringUtils.difference("abc","abg"));

        System.out.println("Abbrevation function tried");
        System.out.println(StringUtils.abbreviate("", 4));
        System.out.println(StringUtils.abbreviate("abcdefg",6));
        System.out.println(StringUtils.abbreviate("abcdefg",7));
        System.out.println(StringUtils.abbreviate("abcdefg",8));
        System.out.println(StringUtils.abbreviate("abcdefg",4));
        //System.out.println(StringUtils.abbreviate("abcdefg",3));  // illegal argument exception

        System.out.println("Abbreviate with offset");
        System.out.println(StringUtils.abbreviate("abcdefghijklmno",1,10));


        System.out.println(StringUtils.abbreviate("" , "...", 4));
        System.out.println(StringUtils.abbreviate("abcdefg" , ".", 5));
        System.out.println(StringUtils.abbreviate("abcdefg" , ".", 7));
        System.out.println(StringUtils.abbreviate("abcdefg" , ".", 8));
        System.out.println(StringUtils.abbreviate("abcdefg" , "..", 4));
        System.out.println(StringUtils.abbreviate("abcdefg" , "..", 3));


        System.out.println(StringUtils.appendIfMissing("null",null,null));
        System.out.println(StringUtils.appendIfMissing("abc",null,null));
        System.out.println(StringUtils.appendIfMissing(" ","xyz",null));
        System.out.println(StringUtils.appendIfMissing("abc","xyz",new CharSequence[]{null}));
        System.out.println("check this output");
        System.out.println(StringUtils.appendIfMissing("abc","xyz",""));
        System.out.println(StringUtils.appendIfMissing("abc","xyz",null));
        System.out.println(StringUtils.appendIfMissing("abcxyz","xyz","mno"));
        System.out.println(StringUtils.appendIfMissing("abc","abc"," "));


        System.out.println("Capitalize only catilizes the first world");
        System.out.println(StringUtils.capitalize(null));
        System.out.println(StringUtils.capitalize(""));
        System.out.println(StringUtils.capitalize("cat"));
        System.out.println(StringUtils.capitalize("cAt"));
        System.out.println(StringUtils.capitalize("'cat'"));

        System.out.println("Center function ");

        System.out.println(StringUtils.center("avc",7));
        System.out.println(StringUtils.center("abcd",8));
        System.out.println(StringUtils.center("a",4));
        System.out.println(StringUtils.center("ab", 4)); //
        System.out.println(StringUtils.center("ab", -1));  //negative is treated as zero
        System.out.println(StringUtils.center("abcd", 2));
        //If the size is less than length original string is returned

        System.out.println(StringUtils.compare(null,null));
        System.out.println(StringUtils.compare(null,"a"));
        System.out.println(StringUtils.compare("a",null));
        System.out.println(StringUtils.compare("a","b"));
        System.out.println(StringUtils.compare("b","a"));
        System.out.println(StringUtils.compare("a","B"));  //31 is output
        System.out.println(StringUtils.compare("ab","abc")); //<0== -1 && >0 ==1
        System.out.println(StringUtils.compare("abc","abc"));// =0


        System.out.println("Method count matches");
        System.out.println(StringUtils.countMatches("abc","a"));  //output is 1
        System.out.println(StringUtils.countMatches("abc","null")); //0
        System.out.println(StringUtils.countMatches("abc","x"));
        System.out.println(StringUtils.countMatches("bbb","b"));


        System.out.println(StringUtils.difference("abc","xyz"));
        System.out.println(StringUtils.difference("I am a machine","I am a robot")); //String2-string 1
        //Output is robot

    }


}
